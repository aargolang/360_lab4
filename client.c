// The echo client client.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>

#include <sys/socket.h>
#include <netdb.h>

#define MAX 256
#define h_addr h_addr_list[0] /* for backward compatibility */

// Define variables
struct hostent *hp;
struct sockaddr_in server_addr;

int server_sock, r;
int SERVER_IP, SERVER_PORT;

int lmkdir(char* pathname); 
int lrmdir(char* pathname); 
int lls(char* pathname);
int lcd(char* pathname); 
int lpwd(char* pathname); 
int lrm(char* pathname);
int get(char* pathname);
int put(char* pathname);
int quit(char* pathname);            

char *cmd[] = {"lmkdir", "lrmdir", "lls", "lcd", "lpwd", "lrm", NULL};
int (*fptr[])(char *) = {(int (*)())lmkdir, lrmdir, lls, lcd, lpwd, lrm, get, put, quit};

// clinet initialization code

int client_init(char *argv[])
{
   printf("======= clinet init ==========\n");

   printf("1 : get server info\n");
   hp = gethostbyname(argv[1]);
   if (hp == 0)
   {
      printf("unknown host %s\n", argv[1]);
      exit(1);
   }

   SERVER_IP = *(long *)hp->h_addr;
   SERVER_PORT = atoi(argv[2]);

   printf("2 : create a TCP socket\n");
   server_sock = socket(AF_INET, SOCK_STREAM, 0);
   if (server_sock < 0)
   {
      printf("socket call failed\n");
      exit(2);
   }

   printf("3 : fill server_addr with server's IP and PORT#\n");
   server_addr.sin_family = AF_INET;
   server_addr.sin_addr.s_addr = SERVER_IP;
   server_addr.sin_port = htons(SERVER_PORT);

   // Connect to server
   printf("4 : connecting to server ....\n");
   r = connect(server_sock, (struct sockaddr *)&server_addr, sizeof(server_addr));
   if (r < 0)
   {
      printf("connect failed\n");
      exit(1);
   }

   printf("5 : connected OK to \007\n");
   printf("---------------------------------------------------------\n");
   printf("hostname=%s  IP=%s  PORT=%d\n",
          hp->h_name, inet_ntoa(SERVER_IP), SERVER_PORT);
   printf("---------------------------------------------------------\n");

   printf("========= init done ==========\n");
}

int lpwd(char *pathname)
{
   char* buf[MAX];
   getcwd(buf, MAX);

   printf("pwd=%s\n", buf);

   return 0;
}
int lls(char *pathname)
{
   char buf[MAX];
   getcwd(buf, MAX);

   printf("pathname=%s\n",pathname);
   
   strcat(buf, pathname);

   printf("buf=%s\n",buf);

   DIR *dp = NULL;
   dp = opendir(buf);
   if (dp){
      printf("yay\n");
   }
   else {
      printf("error: %s\n", strerror(errno));
   }

   printf("ls\n");
   return 0;
}
int lcd(char *pathname)
{

   int r = chdir(pathname);
   char result[MAX];;
   strcat(result, "cd ");
   strcat(result, pathname);
   if (r == 0){
      strcat(result, " OK");
      return 0;
   }
   else if (r == -1){
      printf("error: %s\n", strerror(errno));
   }
   //printf("result\n");
   return 0;
}
int lmkdir(char *pathname)
{
   printf("mkdir\n");
   return 0;
}
int lrmdir(char *pathname)
{
   printf("rmdir\n");
   return 0;
}
int lrm(char *pathname)
{
   printf("rm\n");
   return 0;
}
int get(char *pathname)
{
   printf("get\n");
   return 0;
}
int put(char *pathname)
{
   printf("put\n");
   return 0;
}
int quit(char *pathname)
{
   printf("quit\n");
   return 0;
}       

int findCmd(char* command)
{
	for(int i = 0; cmd[i]; i++){
		if(strcmp(command, cmd[i])==0)
			return i;
	}
	return -1;
}
 

int main(int argc, char *argv[])
{
   int n, functionIndex;
   char line[MAX], ans[MAX], command[MAX], pathname[MAX];


   if (argc < 3)
   {
      printf("Usage : client ServerName SeverPort\n");
      exit(1);
   }

   client_init(argv);
   // sock <---> server
   printf("********  processing loop  *********\n");
   while (1)
   {
      printf("input a line : ");
      bzero(line, MAX);        // zero out line[ ]
      fgets(line, MAX, stdin); // get a line (end with \n) from stdin

      line[strlen(line) - 1] = 0; // kill \n at end
      if (line[0] == 0)           // exit if NULL line
         exit(0);

      sscanf(line, "%s %s", command, pathname);
      printf("pathname length %d", strlen(pathname));

      // printf("%s", command);
      functionIndex = findCmd(command);

      if (functionIndex > 0){
         fptr[functionIndex](pathname); // removed function return value
      }
      /*

      // Send ENTIRE line to server
      n = write(server_sock, line, MAX);
      printf("client: wrote n=%d bytes; line=(%s)\n", n, line);

      // Read a line from sock and show it
      n = read(server_sock, ans, MAX);
      printf("client: read  n=%d bytes; echo=(%s)\n", n, ans);
      */
   }
}
